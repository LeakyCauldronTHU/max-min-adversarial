import tensorflow as tf
import functools

from baselines.common.tf_util import get_session, save_variables, load_variables
from baselines.common.tf_util import initialize

try:
    from baselines.common.mpi_adam_optimizer import MpiAdamOptimizer
    from mpi4py import MPI
    from baselines.common.mpi_util import sync_from_root
except ImportError:
    MPI = None

import numpy as np


class Model(object):
    """
    We use this object to :
    __init__:
    - Creates the step_model
    - Creates the train_model

    train():
    - Make the training part (feedforward and retropropagation of gradients)

    save/load():
    - Save load the model
    """
    def __init__(self, *, policy, ob_space, ac_space, nbatch_act, nbatch_train,
                nsteps, ent_coef, vf_coef, max_grad_norm, attack_args=None, mpi_rank_weight=1, comm=None, microbatch_size=None):
        self.sess = sess = get_session()
        self.attack_args=attack_args

        if MPI is not None and comm is None:
            comm = MPI.COMM_WORLD

        with tf.variable_scope('ppo2_model', reuse=tf.AUTO_REUSE):
            # CREATE OUR TWO MODELS
            # act_model that is used for sampling
            act_model = policy(nbatch_act, 1, sess)

            # Train model for training
            if microbatch_size is None:
                train_model = policy(nbatch_train, nsteps, sess)
            else:
                train_model = policy(microbatch_size, nsteps, sess)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])
        # Keep track of old actor
        self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
        # Keep track of old critic
        self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])
        self.LR_FOR_ATTACK = LR_FOR_ATTACK = tf.placeholder(tf.float32, [])
        # Cliprange
        self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])

        neglogpac = train_model.pd.neglogp(A)

        # Calculate the entropy
        # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
        entropy = tf.reduce_mean(train_model.pd.entropy())

        # value loss
        vpred = train_model.vf
        vpredclipped = OLDVPRED + tf.clip_by_value(train_model.vf - OLDVPRED, - CLIPRANGE, CLIPRANGE)
        vf_losses1 = tf.square(vpred - R)
        vf_losses2 = tf.square(vpredclipped - R)
        vf_loss = .5 * tf.reduce_mean(tf.maximum(vf_losses1, vf_losses2))

        # policy loss (for adversarial and for normal)
        ratio = tf.exp(OLDNEGLOGPAC - neglogpac)
        pg_losses_normal_1 = -ADV * ratio
        pg_losses_normal_2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)
        pg_loss_normal = tf.reduce_mean(tf.maximum(pg_losses_normal_1, pg_losses_normal_2)) - entropy * ent_coef

        pg_losses_adversarial_1 = ADV * ratio
        pg_losses_adversarial_2 = ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)
        pg_loss_adversarial = tf.reduce_mean(tf.maximum(pg_losses_adversarial_1, pg_losses_adversarial_2)) - entropy * ent_coef

        # the statistical evaluation of the kl divergence between \pi_old and \pi
        approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
        clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

        # UPDATE THE PARAMETERS USING LOSS
        # 1. Get the model parameters
        params_pi = tf.trainable_variables('ppo2_model/pi')
        params_at = tf.trainable_variables('ppo2_model/at')
        params_vf = tf.trainable_variables('ppo2_model/vf')
        # print("params_at", params_at)
        # 2. Build our trainer
        if comm is not None and comm.Get_size() > 1:
            self.trainer_pi = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
            self.trainer_at = MpiAdamOptimizer(comm, learning_rate=LR_FOR_ATTACK, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
            self.trainer_vf = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        else:
            self.trainer_pi = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
            self.trainer_at = tf.train.AdamOptimizer(learning_rate=LR_FOR_ATTACK, epsilon=1e-5)
            self.trainer_vf = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
        # 3. Calculate the gradients
        grads_and_var_pi = self.trainer_pi.compute_gradients(pg_loss_normal, params_pi)
        grads_and_var_at = self.trainer_at.compute_gradients(pg_loss_adversarial, params_at)
        grads_and_var_vf = self.trainer_vf.compute_gradients(vf_loss, params_vf)
        grads_pi, var_pi = zip(*grads_and_var_pi)
        grads_at, var_at = zip(*grads_and_var_at)
        grads_vf, var_vf = zip(*grads_and_var_vf)

        if max_grad_norm is not None:
            # Clip the gradients (normalize)
            grads_pi, _grad_norm = tf.clip_by_global_norm(grads_pi, max_grad_norm)
            grads_at, _grad_norm = tf.clip_by_global_norm(grads_at, max_grad_norm)
            grads_vf, _grad_norm = tf.clip_by_global_norm(grads_vf, max_grad_norm)
        grads_and_var_pi = list(zip(grads_pi, var_pi))
        grads_and_var_at = list(zip(grads_at, var_at))
        grads_and_var_vf = list(zip(grads_vf, var_vf))

        self._train_op_pi = self.trainer_pi.apply_gradients(grads_and_var_pi)
        self._train_op_at = self.trainer_at.apply_gradients(grads_and_var_at)
        self._train_op_vf = self.trainer_vf.apply_gradients(grads_and_var_vf)

        if attack_args.training_type == 'both':
            self.loss_names = ['policy_loss_pi', 'policy_loss_at', 'value_loss', 'policy_entropy', 'approxkl', 'clipfrac']
            self.stats_list = [pg_loss_normal, pg_loss_adversarial, vf_loss, entropy, approxkl, clipfrac]
        if attack_args.training_type == 'attack' or attack_args.training_type == 'further':
            self.loss_names = ['policy_loss_at', 'value_loss', 'policy_entropy', 'approxkl', 'clipfrac']
            self.stats_list = [pg_loss_adversarial, vf_loss, entropy, approxkl, clipfrac]
        if attack_args.training_type == 'normal':
            self.loss_names = ['policy_loss_pi', 'value_loss', 'policy_entropy', 'approxkl', 'clipfrac']
            self.stats_list = [pg_loss_normal, vf_loss, entropy, approxkl, clipfrac]

        self.stats_list_for_dynamic = [approxkl, clipfrac]

        self.train_model = train_model
        self.act_model = act_model
        self.step = act_model.step
        self.step_free = act_model.step_free
        self.step_policy_adversaries = act_model.step_policy_adversaries
        self.get_adversaries = act_model.get_adversaries
        self.get_other_adversaries = act_model.get_other_adversaries
        self.value = act_model.value
        self.initial_state = act_model.initial_state

        self.save = functools.partial(save_variables, sess=sess)
        self.load = functools.partial(load_variables, sess=sess, scope=None)

        initialize()

        global_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="")
        if MPI is not None:
            sync_from_root(sess, global_variables, comm=comm) #pylint: disable=E1101

    def train(self, update, lr_for_attack_now, lr, cliprange_shrink, cliprange, obs, returns, masks, actions, values,
              neglogpacs, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        advs = returns - values
        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)

        td_map_shrink = {
            self.train_model.X : obs,
            self.A : actions,
            self.ADV : advs,
            self.R : returns,
            self.LR : lr,
            self.LR_FOR_ATTACK: lr_for_attack_now,
            self.CLIPRANGE : cliprange_shrink,
            self.OLDNEGLOGPAC : neglogpacs,
            self.OLDVPRED : values
        }

        td_map = {
            self.train_model.X: obs,
            self.A: actions,
            self.ADV: advs,
            self.R: returns,
            self.LR: lr,
            self.LR_FOR_ATTACK: lr_for_attack_now,
            self.CLIPRANGE: cliprange,
            self.OLDNEGLOGPAC: neglogpacs,
            self.OLDVPRED: values
        }

        approxkl, clipfrac = None, None
        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks

        if self.attack_args.training_type == 'both':
            if self.attack_args.alternate:
                assert np.abs(self.attack_args.attack_interval) > 1, 'the attack interval must be bigger than 1'
                if update == 1:
                    print("using alternate updating scheme")
                assert self.attack_args.use_attack, 'the training type is both, but use_attack is False'
                if self.attack_args.attack_interval > 0:
                    if update == 1 and np.abs(self.attack_args.attack_interval) > 1:
                        print("training type is both, normal training conquers")
                    if np.mod(update, self.attack_args.attack_interval) == 0:
                        self.sess.run([self._train_op_vf, self._train_op_at], td_map)
                    else:
                        self.sess.run([self._train_op_vf, self._train_op_pi], td_map_shrink)
                        approxkl, clipfrac = self.sess.run(self.stats_list_for_dynamic, td_map_shrink)

                    return self.sess.run(self.stats_list, td_map), [approxkl, clipfrac]

                if self.attack_args.attack_interval < 0:
                    if update == 1 and np.abs(self.attack_args.attack_interval) > 1:
                        print("training type is both, attack training conquers")
                    if np.mod(update, -self.attack_args.attack_interval) == 0:
                        self.sess.run([self._train_op_vf, self._train_op_pi], td_map_shrink)
                        approxkl, clipfrac = self.sess.run(self.stats_list_for_dynamic, td_map_shrink)
                    else:
                        self.sess.run([self._train_op_vf, self._train_op_at], td_map)

                    return self.sess.run(self.stats_list, td_map), [approxkl, clipfrac]
            else:
                if update == 1:
                    print("using simultaneous updating scheme")
                assert self.attack_args.use_attack, 'the training type is both, but use_attack is False'

                self.sess.run([self._train_op_vf], td_map)

                if self.attack_args.attack_interval > 0:
                    if update == 1 and np.abs(self.attack_args.attack_interval) > 1:
                        print("training type is both, normal training conquers")
                    self.sess.run([self._train_op_pi], td_map_shrink)
                    approxkl, clipfrac = self.sess.run(self.stats_list_for_dynamic, td_map_shrink)
                    if np.mod(update, self.attack_args.attack_interval) == 0:
                        self.sess.run([self._train_op_at], td_map)

                if self.attack_args.attack_interval < 0:
                    if update == 1 and np.abs(self.attack_args.attack_interval) > 1:
                        print("training type is both, attack training conquers")
                    self.sess.run([self._train_op_at], td_map)
                    if np.mod(update, -self.attack_args.attack_interval) == 0:
                        approxkl, clipfrac = self.sess.run(self.stats_list_for_dynamic, td_map_shrink)
                        self.sess.run([self._train_op_pi], td_map_shrink)

                return self.sess.run(self.stats_list, td_map), [approxkl, clipfrac]

        elif self.attack_args.training_type == 'normal':
            assert not self.attack_args.use_attack, 'the training type is normal, but use_attack is True'
            self.sess.run([self._train_op_vf] + [self._train_op_pi], td_map)
            return self.sess.run(self.stats_list, td_map)

        elif self.attack_args.training_type == 'attack' or self.attack_args.training_type == 'further':
            assert self.attack_args.use_attack, 'the training type is attack, but use_attack is False'
            self.sess.run([self._train_op_vf] + [self._train_op_at], td_map)
            return self.sess.run(self.stats_list, td_map)


