# coding=utf-8
import os
import io
import subprocess
import json
import csv

from datetime import datetime


cluster = 'gcr'

if __name__ == '__main__':
    path_logs = r'//philly/' + cluster + '/resrchvc/v-wancha/mnist_logs'
    path_user = r'//philly/' + cluster + '/resrchvc/v-wancha/'

    logs = os.listdir(path_logs)
    for lg in logs:
        print("lg", lg)
        if "coe_hid=0.0001" in lg and "coe_nor=1.0" in lg:

            cmd = 'rm -rf '
            cmd += path_logs
            cmd += '/'
            cmd += lg
            print(cmd)
            subprocess.call(cmd, shell=True)


