import sys
import re
import multiprocessing
import os.path as osp
import gym
from collections import defaultdict
import tensorflow as tf
import numpy as np

from baselines.common.vec_env import VecFrameStack, VecNormalize, VecEnv
from baselines.common.vec_env.vec_video_recorder import VecVideoRecorder
from baselines.common.cmd_util import common_arg_parser, parse_unknown_args, make_vec_env, make_env, attack_arg_parser
from baselines.common.tf_util import get_session
from baselines import logger
from importlib import import_module

import os
import subprocess
import datetime
import matplotlib.pyplot as plt

try:
    from mpi4py import MPI
except ImportError:
    MPI = None

try:
    import pybullet_envs
except ImportError:
    pybullet_envs = None

try:
    import roboschool
except ImportError:
    roboschool = None

_game_envs = defaultdict(set)
for env in gym.envs.registry.all():
    # TODO: solve this with regexes
    env_type = env._entry_point.split(':')[0].split('.')[-1]
    _game_envs[env_type].add(env.id)

# reading benchmark names directly from retro requires
# importing retro here, and for some reason that crashes tensorflow
# in ubuntu
_game_envs['retro'] = {
    'BubbleBobble-Nes',
    'SuperMarioBros-Nes',
    'TwinBee3PokoPokoDaimaou-Nes',
    'SpaceHarrier-Nes',
    'SonicTheHedgehog-Genesis',
    'Vectorman-Genesis',
    'FinalFight-Snes',
    'SpaceInvaders-Snes',
}


def train(args, attack_args, extra_args):
    env_type, env_id = get_env_type(args)
    print('env_type: {}'.format(env_type))

    total_timesteps = int(args.num_timesteps)
    seed = args.seed

    learn = get_learn_function(args.alg)
    alg_kwargs = get_learn_function_defaults(args.alg, env_type)

    alg_kwargs['ent_coef'] = args.ent_coef
    alg_kwargs.update(extra_args)
    lr_for_attack_tmp = np.copy(attack_args.lr_for_attack)
    attack_args.lr_for_attack = lambda f: f * lr_for_attack_tmp

    env = build_env(args)
    if attack_args.use_attack == 1.0:
        eval_env = build_env(args)
        # eval_env = None
        # eval_env_free = build_env(args)
        eval_env_free = None
    else:
        eval_env = None
        eval_env_free = None
    if args.save_video_interval != 0:
        env = VecVideoRecorder(env, osp.join(logger.get_dir(), "videos"), record_video_trigger=lambda x: x % args.save_video_interval == 0, video_length=args.save_video_length)

    if args.network:
        alg_kwargs['network'] = args.network
    else:
        if alg_kwargs.get('network') is None:
            alg_kwargs['network'] = get_default_network(env_type)

    print('Training {} on {}:{} with arguments \n{}'.format(args.alg, env_type, env_id, alg_kwargs))
    print("alg_kwrgs", alg_kwargs)

    model = learn(
        env=env,
        eval_env = eval_env,
        eval_env_free = eval_env_free,
        seed=seed,
        total_timesteps=total_timesteps,
        attack_args=attack_args,
        **alg_kwargs
    )

    return model, env


def build_env(args, env_type='train'):
    ncpu = multiprocessing.cpu_count()
    if sys.platform == 'darwin': ncpu //= 2
    nenv = args.num_env or ncpu
    alg = args.alg
    seed = args.seed

    env_type, env_id = get_env_type(args)

    if env_type in {'atari', 'retro'}:
        if alg == 'deepq':
            env = make_env(env_id, env_type, seed=seed, wrapper_kwargs={'frame_stack': True})
        elif alg == 'trpo_mpi':
            env = make_env(env_id, env_type, seed=seed)
        else:
            frame_stack_size = 4
            env = make_vec_env(env_id, env_type, nenv, seed, gamestate=args.gamestate, reward_scale=args.reward_scale)
            env = VecFrameStack(env, frame_stack_size)

    else:
        config = tf.ConfigProto(allow_soft_placement=True,
                               intra_op_parallelism_threads=1,
                               inter_op_parallelism_threads=1)
        config.gpu_options.allow_growth = True
        get_session(config=config)

        flatten_dict_observations = alg not in {'her'}
        env = make_vec_env(env_id, env_type, args.num_env or 1, seed, reward_scale=args.reward_scale, flatten_dict_observations=flatten_dict_observations)

        if env_type == 'mujoco':
            env = VecNormalize(env, use_tf=True)

    return env


def get_env_type(args):
    env_id = args.env

    if args.env_type is not None:
        return args.env_type, env_id

    # Re-parse the gym registry, since we could have new envs since last time.
    for env in gym.envs.registry.all():
        env_type = env._entry_point.split(':')[0].split('.')[-1]
        _game_envs[env_type].add(env.id)  # This is a set so add is idempotent

    if env_id in _game_envs.keys():
        env_type = env_id
        env_id = [g for g in _game_envs[env_type]][0]
    else:
        env_type = None
        for g, e in _game_envs.items():
            if env_id in e:
                env_type = g
                break
        if ':' in env_id:
            env_type = re.sub(r':.*', '', env_id)
        assert env_type is not None, 'env_id {} is not recognized in env types'.format(env_id, _game_envs.keys())

    return env_type, env_id


def get_default_network(env_type):
    if env_type in {'atari', 'retro'}:
        return 'cnn'
    else:
        return 'mlp'

def get_alg_module(alg, submodule=None):
    submodule = submodule or alg
    try:
        # first try to import the alg module from baselines
        alg_module = import_module('.'.join(['baselines', alg, submodule]))
    except ImportError:
        # then from rl_algs
        alg_module = import_module('.'.join(['rl_' + 'algs', alg, submodule]))

    return alg_module


def get_learn_function(alg):
    return get_alg_module(alg).learn


def get_learn_function_defaults(alg, env_type):
    try:
        alg_defaults = get_alg_module(alg, 'defaults')
        kwargs = getattr(alg_defaults, env_type)()
    except (ImportError, AttributeError):
        kwargs = {}
    return kwargs


def parse_cmdline_kwargs(args):
    '''
    convert a list of '='-spaced command-line arguments to a dictionary, evaluating python objects when possible
    '''
    def parse(v):

        assert isinstance(v, str)
        try:
            return eval(v)
        except (NameError, SyntaxError):
            return v

    return {k: parse(v) for k,v in parse_unknown_args(args).items()}


def str_process(strings):
    components = strings.split('_')
    short = ''
    for i in range(len(components)):
        short += components[i][0:3]
        short += '_'
    short = short[:-1]
    return short


def env_process(env):
    if isinstance(env, str):
        if 'NoFrameskip' in env:
            env = env.split('NoFrameskip')[0]

    return env


def delete_old_logs(dir, logs):
    print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    try:
        existing_logs = os.listdir(dir)
        print("searching old logs")
        for lg in existing_logs:
            if logs in lg:
                print("we have found old logs:", lg, "which is the same as:", logs)
                cmd = 'rm -rf ' + os.path.join(dir, lg)
                print('delete command is', cmd)
                subprocess.call(cmd, shell=True)
            else:
                pass

    except:
        print("the log directory does not exist", dir)
        pass
    print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")


def find_logs(philly_path, log_name):
    logs = os.listdir(philly_path)

    log_path = ''
    # print("log_name", log_name)
    # print("logs", logs)
    for log in logs:
        # print("log", log)
        if log_name in log:
            print("found normally trained models:", log, "which is the same as:", log_name)
            model = (os.listdir(os.path.join(philly_path, log, 'checkpoints')))
            print("models", model)
            if len(model) >= 4:
                model = model[-1]
                log_path = os.path.join(philly_path, log, 'checkpoints', model)
            break
    if log_path:
        return log_path
    else:
        print("normally trained models not found!")
        return None


def main(args):
    # processing args ##################################################################################################
    args_sys = args
    arg_parser = common_arg_parser()
    args, unknown_args = arg_parser.parse_known_args(args_sys)
    extra_args_1 = parse_cmdline_kwargs(unknown_args)

    if args.cluster is not None:
        args.num_env = 16
        args.log_path = '//philly/' + args.cluster + '/resrchvc/v-wancha/max_min_logs/'
    if args.play:
        args.num_env = 1
        args.save_video_interval = 1
        args.save_video_length = 10000

    attack_args_parser = attack_arg_parser()
    attack_args, unknown_args = attack_args_parser.parse_known_args(args_sys)
    extra_args_2 = parse_cmdline_kwargs(unknown_args)

    attack_args.lr_for_attack = attack_args.lr_for_attack * attack_args.lr_times
    if attack_args.training_type == 'normal' and not args.play:
        attack_args.use_attack = 0.0
    elif (attack_args.training_type == 'attack' or attack_args.training_type == 'both' or attack_args.training_type == 'further') and not args.play:
        attack_args.use_attack = 1.0
    else:
        pass

    if attack_args.other_attack_type and not args.compare and args.play:
        attack_args.use_attack = 0.0

    extra_args = dict.fromkeys([x for x in extra_args_1 if x in extra_args_2])
    for t in extra_args.keys():
        extra_args[t] = extra_args_1[t]

    # specify load variable scope, need further revision
    # extra_args.update({'load_scope': ['ppo2_model/pi']})
    if not args.play and attack_args.training_type == 'attack':
        extra_args.update({'load_scope': ['ppo2_model/pi']})
    if not args.play and attack_args.training_type == 'further' and attack_args.further_type == 'full':
        extra_args.update({'load_scope': ['ppo2_model/pi', 'ppo2_model/at']})
    if not args.play and attack_args.training_type == 'further' and attack_args.further_type == 'partial':
        extra_args.update({'load_scope': ['ppo2_model/pi']})
    if args.play:
        extra_args.update({'load_scope': ['ppo2_model/pi', 'ppo2_model/at']})
        # extra_args.update({'load_scope': ['ppo2_model/pi']})

    # include args info into log directory
    args_dict, attack_args_dict = args.__dict__, attack_args.__dict__
    infos_args = ['env', 'seed', 'ent_coef']
    if attack_args.training_type == 'both':
        infos_attack_args = ['use_attack', 'training_type', 'attack_interval', 'lr_times', 'stack_attack_network',
                             'clip_shrink', 'clip_magnify']
        if attack_args.dynamic_clip:
            infos_attack_args.append('dynamic_clip')
        if attack_args.alternate:
            infos_attack_args.append('alternate')

    if attack_args.training_type == 'further':
        infos_attack_args = ['use_attack', 'training_type', 'attack_interval', 'lr_times', 'stack_attack_network', 'lr_shrink', 'clip_shrink',
                             'further_type']
        if attack_args.dynamic_clip:
            infos_attack_args.append('dynamic_clip')
        if attack_args.alternate:
            infos_attack_args.append('alternate')
    if attack_args.training_type == 'attack':
        infos_attack_args = ['use_attack', 'training_type', 'stack_attack_network']
    if attack_args.training_type == 'normal':
        infos_attack_args = ['use_attack', 'training_type']

    # infos_extra_args = ['network_size']
    infos_extra_args = []

    extra_args_log = {}
    for i in range(len(infos_extra_args)):
        if infos_extra_args[i] not in extra_args.keys():
            extra_args_log.update({infos_extra_args[i]: None})
    extra_args_log.update(extra_args)

    dir_tmp = ''
    if args.log_path is None:
        args.log_path = '~'

    for i in range(len(infos_args)):
        dir_tmp = dir_tmp + str_process(infos_args[i]) + '=' + str(env_process(args_dict[infos_args[i]])) + '-'
    for i in range(len(infos_attack_args)):
        dir_tmp = dir_tmp + str_process(infos_attack_args[i]) + '=' + str(attack_args_dict[infos_attack_args[i]]) + '-'
    for i in range(len(infos_extra_args)):
        dir_tmp = dir_tmp + str_process(infos_extra_args[i]) + '=' + str(extra_args_log[infos_extra_args[i]]) + '-'

    delete_old_logs(args.log_path, dir_tmp)

    if attack_args.training_type == 'attack' and not args.play and args.cluster:
        load_path = r'//philly/' + args.cluster + r'/resrchvc/v-wancha/max_min_logs'
        dir_tmp_tmp = dir_tmp.replace('tra_typ=attack', 'tra_typ=normal')
        dir_tmp_tmp = dir_tmp_tmp.replace('use_att=1.0', 'use_att=0.0')
        dir_tmp_tmp = dir_tmp_tmp.replace('-sta_att_net=' + str(attack_args.stack_attack_network), '')

        print("find logs", dir_tmp_tmp)
        load_path = find_logs(load_path, dir_tmp_tmp)
        extra_args.update({'load_path': load_path})

    if attack_args.training_type == 'further' and not args.play and args.cluster:
        load_path = r'//philly/' + args.cluster + r'/resrchvc/v-wancha/max_min_logs'
        dir_tmp_tmp = dir_tmp.replace('tra_typ=further', 'tra_typ=both')
        dir_tmp_tmp = dir_tmp_tmp.replace('-fur_typ=full', '')
        dir_tmp_tmp = dir_tmp_tmp.replace('-fur_typ=partial', '')

        print("find logs", dir_tmp_tmp)
        load_path = find_logs(load_path, dir_tmp_tmp)
        extra_args.update({'load_path': load_path})
        if load_path is None:
            print("the model to be load is not well trained")
            exit(0)

    # if "load_path" in extra_args:
    #     extra_args['load_path'] = ["d:\last", extra_args['load_path']]

    dir_tmp = dir_tmp + datetime.datetime.now().strftime("%m_%d_%H_%M_%S_%f")

    args.log_path = osp.join(args.log_path, dir_tmp)
    if args.log_path[0] == '~':
        args.log_path = osp.expanduser(args.log_path)

    if MPI is None or MPI.COMM_WORLD.Get_rank() == 0:
        rank = 0
        logger.configure(dir=args.log_path)
    else:
        logger.configure(format_strs=[])
        rank = MPI.COMM_WORLD.Get_rank()

    logger.log("*******************common args************************")
    for k in args_dict.keys():
        logger.log(k, args_dict[k])
    logger.log("*******************attack args************************")
    for k in attack_args_dict.keys():
        logger.log(k, attack_args_dict[k])
    logger.log("*******************extra args*************************")
    for k in extra_args.keys():
        logger.log(k, extra_args[k])
    logger.log("******************************************************")
    # finish logs processing ###########################################################################################

    model, env = train(args, attack_args, extra_args)

    # saving the final trained model ###################################################################################
    save_path = osp.join(args.log_path, 'checkpoints', 'last')
    if rank == 0:
        model.save(save_path)
    # finished saving models ###########################################################################################

    ####################################################################################################################
    # begin evaluation #################################################################################################
    if args.play:
        logger.log("Running trained model")
        obs = env.reset()

        episode_rew = 0
        counter = 0
        while True:
            counter += 1
            if attack_args.other_attack_type and not args.compare:
                # print("using other types of attacks")
                adversaries = model.get_other_adversaries(obs)
                actions, _, _, _ = model.step_free(obs+adversaries)
            else:
                # print("using learnt attacks")
                adversaries = model.get_adversaries(obs)
                actions, _, _, _ = model.step(obs)
                # actions, _, _, _ = model.step_free(obs+adversaries)

            if args.plot and np.mod(counter, 100) == 0:
                plt.figure()
                plt.subplot(1,2,1)
                plt.imshow(adversaries[0,:,:,0])
                # plt.colorbar()
                plt.subplot(1,2,2)
                plt.imshow((obs+adversaries)[0,:,:,0])
                plt.show()
                plt.close()

            if args.compare and np.mod(counter, 100) == 0:
                adversaries_other = model.get_other_adversaries(obs)
                adversaries = model.get_adversaries(obs)
                plt.figure()
                plt.subplot(1,3,1)
                plt.imshow(obs[0, :, :, 0])
                # plt.colorbar()
                plt.subplot(1, 3, 2)
                plt.imshow(adversaries_other[0, :, :, 0])
                # plt.colorbar()
                plt.subplot(1, 3, 3)
                plt.imshow(adversaries[0, :, :, 0])
                # plt.colorbar()
                plt.show()
                plt.close()

            obs, rew, done, info = env.step(actions)
            # print("info", done, info)
            episode_rew += rew[0] if isinstance(env, VecEnv) else rew
            env.render()
            done = done.any() if isinstance(done, np.ndarray) else done
            if done:
                print('episode_rew={}'.format(episode_rew))
                episode_rew = 0
                obs = env.reset()

    env.close()

    return model


if __name__ == '__main__':
    main(sys.argv)
